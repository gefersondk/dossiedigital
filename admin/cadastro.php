<?php
  session_start();
  if((!$_SESSION["logado"] || $_SESSION["tipoUser"]!=1))
      header("Location:../login.php");
    header("Content-Type: text/html; charset=utf-8",true);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>IFPB</title>
    <!-- Bootstrap -->
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
 
     <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-image: url("../img/bg.jpg");
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
      #msg{
        margin-top: 7%;
        text-align: center;
      }

    </style>
  </head>
  <body>

      
      <div class="container">

      <form class="form-signin" action="cadastro_banco.php" method="POST">
        <h2 class="form-signin-heading">Cadastro</h2>
        <input type="text" class="input-block-level" placeholder="Usuario" name="nome" required="required">
        <input type="text" class="input-block-level" placeholder="Login" name="login"  required="required">
        <input type="password" class="input-block-level" placeholder="Senha" name="senha"  required="required">
        <input type="password" class="input-block-level" placeholder="Confirmar Senha" name="senha2"  required="required">
        <label class="radio">
            <input type="radio"  name="tipo" value="1">
            <p class="text-info">Usuário Administrador</p>
        </label>
        <label class="radio">
            <input type="radio"  name="tipo" value="0">
            <p class="text-info">Usuário Comum</p>
        </label>
      	<!--<input type="submit"   class="btn btn-large btn-primary" value="voltar" onclick="form.action='gerenciar_usuarios.php'; form.submit()"   />-->
        <button class="btn btn-large btn-primary" type="submit">Cadastrar</button>

      </form>

    </div>

    
      
 
    
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    
  </body>
</html>

