<!DOCTYPE html>
<html lang="en">
  <head>
    <?php
      session_start();
          if (!$_SESSION['logado']){
                    header("Location:../login.php");
          }
    ?>
    <meta charset="utf-8">
    <title>IFPB</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<?php
		include "../functionsPDO.php";
	?>
	<script src="../lightbox/js/jquery-1.11.0.min.js"></script>
	<script src="../lightbox/js/lightbox.min.js"></script>
	<link href="../lightbox/css/lightbox.css" rel="stylesheet" />
    <!-- Le styles -->
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Imagens</li>
              <li class="active"><a href="#">Documento Pessoal</a></li>
              <?php
				  if(isset($_GET['codimg'])){
					    //$dados = imagemPessoalAluno($_GET['codimg']);
            $cont =1;
            $qtd = $_GET['quant'];
            for($i = 0; $i < $qtd; $i++) {
              //$l=base64_encode((string)stream_get_contents($value->imagem)
              echo "<li><table><tr><td><a href='imagem.php?codimg={$_GET['codimg']}&i={$i}' data-lightbox='image-1' data-title='My caption' >Imagem ".($i+1)."</a></td><td><a href='imagem.php?codimg={$_GET['codimg']}&i={$i}' target='_blank'><img src='../img/impres.png' width='50px' style='margin-left:13em;'></img></a></td></tr></table></li>";
              $cont++;
            }


				  }
		           
				  
				?>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <!--<div class="span9">
          <div class="hero-unit">
            <h3>Imagens</h1>
            <p></p>
           
          </div>
          
        </div><span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>

  </body>
</html>
