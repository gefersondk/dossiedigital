<?php
	session_start();
	if((!$_SESSION["logado"] || $_SESSION["tipoUser"]!=1))
			header("Location:../login.php");
		header("Content-Type: text/html; charset=utf-8",true);
		include "../functionsPDO.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>IFPB</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
        
      }
      .sidebar-nav {
        padding: 9px 0;
      }
      #logo{
        width: 20px;
      }
    </style>
    <style type="text/css">
			@import "../media/css/jquery.dataTables.css";
		</style>
	<script src="../media/js/jquery.js"></script>
		<script src="../media/js/jquery.dataTables.js"></script>
		
		<script>
			$(document).ready(function(){
				$("#tabela1").dataTable(
					{
				        "language": {
				            "url": "../media/language/br.txt"
				        }
	    			}
				);
			});
		</script>
    <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    
  </head>

  <body>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Painel</li>
              <li class="active"><a href="#">Gerenciar Usuarios</a></li>
              <li><a href="principal.php">Inicio</a></li>
              <li><a href="cadastro.php" target="_blanck">Cadastrar</a></li>
              <li><a href="sair.php">Sair</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          
          <div class="row-fluid">
            <table id="tabela1"  class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Tipo</th>
							<th>Login</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$lista = listarUsuarios();
							
							foreach ($lista as $dados):
								printf("<tr><td>%s</td><td>%s</td><td>%s</td></tr>",$dados->nome,$dados->tipo,$dados->login);

							endforeach;
							
						?>

					</tbody>
				</table>
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div><!--/.fluid-container-->

  </body>
</html>
