<?php
  session_start();
  if((!$_SESSION["logado"] || $_SESSION["tipoUser"]!=1))
      header("Location:../login.php");
    header("Content-Type: text/html; charset=utf-8",true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo "Bem Vindo ".ucfirst($_SESSION["nome"])?></title>
  <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
       
      }
      #logo{
        width: 20px;
      }
      
    </style>
  <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
  <script type="text/javascript">
		/*
        function busca(){
          
            var xmlhttp;
            var nome = document.getElementById('nome').value;
            var cpf = document.getElementById('cpf').value;
            var matricula = document.getElementById('matricula').value;
            var rg = document.getElementById('rg').value;
            var mae = document.getElementById('mae').value;
            var pai = document.getElementById('pai').value;
            

          
            if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }else{// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById("dados").innerHTML=xmlhttp.responseText;
                    

                }
            }
            xmlhttp.open("GET","busca2.php?nome="+nome+"&matricula="+matricula+"&cpf="+cpf+"&rg="+rg+"&mae="+mae+"&pai="+pai,true);
            xmlhttp.send();

        }*/
    </script>

    <?php include_once '../functionsPDO.php';?>
</head>
<body>

  

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
 

 
      <!-- Tenha certeza de deixar a marca se você quer que ela seja mostrada -->
            <div class="brand"><img src="../img/ifpb-logo.png" id="logo"></div>
 
      <!-- Tudo que você queira escondido em 940px ou menos, coloque aqui -->
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li>
            <a href="principal.php">Início</a>
          </li>
          <li><a href="gerenciar_usuarios.php">Gerenciar Usuarios</a></li>
          <li class="active"><a href="listar_documentos.php">Buscar Dossiês</a></li>
          <li><a href="perfil.php">Perfil</a></li>
          
        </ul>
		<ul class="nav" style="float:right;"><li><a href="sair.php">Sair</a></li></ul>
      </div>
 
    </div>
  </div>
</div>
      </div>

     
      <hr>
      <table class=" table table-striped table-hover">
        <thead>
          
           
        </thead>
     
        <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <?php
                $dadosAluno = getAlunoDossieId($_POST['id']);
                foreach ($dadosAluno as $key) {
                  echo "<li>Aluno: ".$key->nome."</li>";
                  echo "<li>Intituição: ".$key->nomei."</li>";
                  echo "<li>Curso: ".$key->nomec."</li>";
				  echo "<li>Classificação:".$key->classificacao."</li>";
                }
              ?>
            </ul>
          </div><!--/.well -->


           <table class='table table-striped table-hover'>
            <tbody>
                <tr><td>Titulo</td><td>Descrição</td><td>Tipo do Documento</td><td>Quantidade de imagens</td><td>Imagens</td></tr>
                <?php

                  $doc = listarDocumentosDossie($_POST['id']);
                  if(isset($doc))
                    foreach ($doc as $documentos) {
                     echo "<tr><td>{$documentos->titulo}</td><td>{$documentos->descricao}</td><td>{$documentos->tipo}</td><td>{$documentos->qnt}</td><td><a href='carregar_imagens.php?codimg=".$documentos->id."&quant={$documentos->qnt}' target='_blank'><img src=../img/img9.ico width='45px' height='45px'></img></a></td></tr>";
                    }
           
                ?>
              
            </tbody>
          </table>
          
               
               <tbody id="dados">
                 
               </tbody>
           
      
      </table>
      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div> <!-- /container -->
  
</body>
</html>