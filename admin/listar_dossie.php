<?php
  session_start();
  if((!$_SESSION["logado"] || $_SESSION["tipoUser"]!=1))
      header("Location:../login.php");
    header("Content-Type: text/html; charset=utf-8",true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo "Bem Vindo ".ucfirst($_SESSION["nome"])?></title>
  <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
       
      }
      #logo{
        width: 20px;
      }
      
    </style>
  <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
  <script type="text/javascript">
        function busca(){
          
            var xmlhttp;
            var nome = document.getElementById('nome').value;
            var inst = document.getElementById('instituicao');
            var instituicao = inst.options[inst.selectedIndex].text;


            var cur = document.getElementById('curso');
            var curso = cur.options[cur.selectedIndex].text;
       
            

          
            if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }else{// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById("dados").innerHTML=xmlhttp.responseText;
                    

                }
            }
            xmlhttp.open("GET","busca_dossie.php?nome="+nome+"&instituicao="+instituicao+"&curso="+curso,true);
            xmlhttp.send();

        }
    </script>

    <?php include_once '../functionsPDO.php';?>
</head>
<body>

  

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
 
              
 
      <!-- Tenha certeza de deixar a marca se você quer que ela seja mostrada -->
            <div class="brand"><img src="../img/ifpb-logo.png" id="logo"></div>
 
      <!-- Tudo que você queira escondido em 940px ou menos, coloque aqui -->
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li>
            <a href="principal.php">Início</a>
          </li>
          <li><a href="gerenciar_usuarios.php">Gerenciar Usuarios</a></li>
          <li class="active"><a href="listar_dossie.php">Buscar Dossiês</a></li>
          <li><a href="perfil.php">Perfil</a></li>
          
        </ul>
          <ul class="nav" style="float:right;"><li><a href="sair.php">Sair</a></li></ul>
      </div>
 
    </div>
  </div>
</div>
      </div>

     <div class="form-inline" role="form" id="pesquisa">
            
                
                    <!--onkeyup="busca(this.value) pra chamar o metodo ajax-->
                    <input type="text" class="input-medium" id="nome" placeholder="Nome do Aluno" name="nome">
             
                    
                    <label>Intituição</label>
                    <select id='instituicao'>
                      <option value=""></option>
                      <?php
                        $inst = getInstituicoes();
                        foreach ($inst as $key) {

                          echo "<option value={$key->nome} >{$key->nome}</option>";
                        }
                      ?>
                    </select>

                    <label>Curso</label>
                    <select id='curso'>
                      <option value="" ></option>
                      <?php
                        $cursos = getCursos();
                        foreach ($cursos as $key) {
                          echo "<option value={$key->nome} >{$key->nome}</option>";
                        }
                      ?>
                    </select>
      
                <button  type="submit" class="btn btn-default" onclick="busca()">Buscar</button>
            
        </div>
      <hr>
      <table class=" table table-striped table-hover">
        <thead>
          <tr><td>Nome do Aluno</td><td>Nome da Iintituição</td><td>Nome do Curso</td></tr>
           
        </thead>
               
               <tbody id="dados">
                 
               </tbody>
           
      
      </table>
      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div> <!-- /container -->
  
</body>
</html>