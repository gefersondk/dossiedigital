<?php
  session_start();
  if((!$_SESSION["logado"] || $_SESSION["tipoUser"]!=1))
      header("Location:../login.php");
    header("Content-Type: text/html; charset=utf-8",true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo "Bem Vindo ".ucfirst($_SESSION["nome"])?></title>
  <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }

      ul{
        list-style:none;
        list-style-type: none;
        text-decoration: none;
        margin-left: 0px;
      }
      #centro{
        float: right;
      }
      .inp{
        text-align: center;

      }
      .inp input{
        margin: 12px;
      }
      #logo{
        width: 20px;
      }
    </style>

  <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
   
  <script src="../js/jquery-latest.js"></script>
  
 
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script>
	function validaSenha (input){ 
			if (input.value != document.getElementById('txtSenha').value) {
				input.setCustomValidity('Repita a senha corretamente');
			} else {
				input.setCustomValidity('');
			}
	}
  </script>
</head>
<body>

  <?php include "../functionsPDO.php";?>

  

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
 
              
 
      <!-- Tenha certeza de deixar a marca se você quer que ela seja mostrada -->
            <div class="brand"><img src="../img/ifpb-logo.png" id="logo"></div>
 
      <!-- Tudo que você queira escondido em 940px ou menos, coloque aqui -->
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li>
            <a href="principal.php">Início</a>
          </li>
           <li><a href="gerenciar_usuarios.php">Gerenciar Usuarios</a></li>
          <li><a href="listar_dossie.php">Buscar Dossiês</a></li>
          <li class="active"><a href="perfil.php">Perfil</a></li>
        </ul>
        <ul class="nav" style="float:right;"><li><a href="sair.php">Sair</a></li></ul>
      </div>
 
    </div>
  </div>
</div>
      </div>
      <div class="container">
        <ul>
        
          <li><div id="centro"><button type="button" data-toggle="modal" data-target="#myModal">Editar Informações</button></div></li>
           
        </ul>
  </div>
  <br>
<div class="container">
        <ul>
          <?php
            $dados = getUsuarioPorId($_SESSION['id']);

            echo "<li><div class='alert alert-info'>Nome: {$dados[0]->nome}</div></li>";
            //echo "<li><div class='alert alert-info'>Email: usuario@gmail.com</div></li>";
            //echo "<li><div class='alert alert-info'>Senha: senhaUsuario</div></li>";
          
          ?>
        </ul>
  </div>

  <!-- Modal Informações-->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" class='inp'>Editar informações básicas</h3>
  </div>
  <form method='post' action="editar_perfil.php">
  <div class="modal-body">
    <?php

      

    
        //echo "<div class='container-fluid inp' ><input type='text' placeholder='Nome' required='required' value='{$dados[0]->nome}'>";
        //echo "<input type='text' placeholder='Email' required='required' value='email do usuario'>";
        echo "<input id='txtSenha' type='password' placeholder='Senha' required='required' name='senha'>";
        echo "<input name='repetir_senha' type='password' placeholder='Confirma Senha' required='required' oninput='validaSenha(this)' pattern='{5}'>";
		
		
		
      
    ?>
	
	
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-primary">Salvar mudanças</button>
  </div>
  </form>
</div>
 
      <hr>

      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div> <!-- /container -->
  
</body>
</html>