<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 1) || (!$_SESSION['logado']))
  if((!$_SESSION["logado"] || $_SESSION["tipoUser"]!=1))
      header("Location:../login.php");
    header("Content-Type: text/html; charset=utf-8",true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo "Bem Vindo ".ucfirst($_SESSION["nome"])?></title>
  <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
        
      }

      #logo{
        width: 20px;
      }
    </style>
  <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>

  

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
 
              
 
      <!-- Tenha certeza de deixar a marca se você quer que ela seja mostrada -->
            <div class="brand"><img src="../img/ifpb-logo.png" id="logo"></div>
 
      <!-- Tudo que você queira escondido em 940px ou menos, coloque aqui -->
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li class="active">
            <a href="principal.php">Início</a>
          </li>
          <li><a href="gerenciar_usuarios.php">Gerenciar Usuarios</a></li>
          <li><a href="listar_dossie.php">Buscar Dossiês</a></li>
          <li><a href="perfil.php">Perfil</a></li>
          
        </ul>
          <ul class="nav" style="float:right;"><li><a href="sair.php">Sair</a></li></ul>
      </div>
 
    </div>
  </div>
</div>
      </div>

      
      <hr>

      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div> <!-- /container -->
  
</body>
</html>