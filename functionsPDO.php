<?php 
@session_start();

$PATH = $_SERVER['DOCUMENT_ROOT'];
$PATH .= "/dossiedigital";
include_once $PATH.'/conexao.php';
header("Content-Type: text/html; charset=utf-8",true);


function cadastrarUsuario($dados = array()){
	$pdo = conectarComPdo();
        if(verificaExistencia($dados['login'])){
            echo "<div class='alert alert-error'>Usuario com login ja cadastrado</div>";
            return 0;
        }
        if(is_array($dados)):
            try{
                $cadastrar = $pdo->prepare("INSERT INTO Usuario (login,nome,senha,tipo) VALUES (:login, :nome, :senha , :tipo)");
                
                foreach($dados as $i=>$j):
                    $cadastrar->bindValue(":$i", $j);
                endforeach;
         
                $cadastrar->execute();
                
                if($cadastrar->rowCount()==1):
                    return true;
                else:
                    return false;
                endif;
            } catch (PDOException $e) {
                echo "<div class='alert alert-error'>Erro ao cadastrar Usuario na base de dados<br></div>";
            }   
        endif;
}

function criptografar($senha){
    //return hash('md5', $senha); 
    return hash('sha256', $senha);
}

function verificarUsuario($login,$senha){
    $nSenha = criptografar($senha);
    if(verificaExistencia($login)){
        $dadosUsuario = getusuarioPorLogin($login);
        if($dadosUsuario[0]->senha == $nSenha){
            $_SESSION['nome']= $dadosUsuario[0]->nome;
        return true;
        }
    }
        
    return false;

}

//recupera o tipo e o id do usuario
function verificarTipo($login){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT id, tipo FROM USUARIO WHERE login LIKE :login ");
        $listar->bindValue(":login",$login);//."%"
        $listar->execute();
        
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}


function getusuarioPorLogin($login){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT login,nome,senha FROM USUARIO WHERE login LIKE :login ");
        $listar->bindValue(":login",$login);//."%"
        $listar->execute();
        
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}

function getUsuarioPorId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT nome FROM USUARIO WHERE id = :id ");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}

function UpdateUsuario($nome,$senha){
    $pdo = conectarComPdo();
    $novaSenha = criptografar($senha);
    try{
        $listar = $pdo->prepare("UPDATE usuario SET nome = :nome, senha = :senha WHERE id = :id; ");
        $listar->bindValue(":nome",$id);//."%"
         $listar->bindValue(":senha",$novaSenha);
        $listar->execute();
        
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}

function verificaExistencia($login){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT login FROM USUARIO WHERE login LIKE :login ");
        $listar->bindValue(":login",$login);//."%"
        $listar->execute();

        if($listar->rowCount()>0):
            return true;
        
        endif;
        return false;
        
    } catch (PDOException $e) {
        echo "Erro ao verificar a existencia do usuario<br>";
    }
}

function listarDocumentosComPdo(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Documentos ORDER BY nome");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os filmes<br>";
    }
}

function listarUsuarios(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Usuario ORDER BY nome");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os Usuarios<br>";
    }
}






function listarDocumentosPorTitulo($titulo){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT * FROM Aluno a  join DocumentoAcademico da on a.id = da.aluno_id join DocumentoPessoal dp on dp.aluno_id = a.id WHERE a.nome LIKE  :titulo ");
        $listar->bindValue(":titulo",$titulo.'%');
        $listar->execute();
        
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        print_r($e);
        echo "Erro ao listar os documentos<br>";
    }

}

function listarDocumentosSimples($nome ='%',$matricula ='%',$rg ='%',$cpf ='%'){

    $pdo = conectarComPdo();
    try{
       
        $listar = $pdo->prepare("SELECT distinct d.id,m.nome,m.tipodocumento,u.login,d.dataleitura,count(img.doc_id) from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Aluno a on m.aluno_id=a.id join 
        Curso c on m.curso_id=c.id join Instituicao i on m.instituicao_id = i.id join Imagem img on d.id=img.doc_id join Usuario u on u.id = d.user_id where a.nome like :nome and a.matricula like :matricula and a.rg like :rg and
        a.cpf like :cpf group by d.id,m.id,img.doc_id,u.id");
        $listar->bindValue(":nome",$nome.'%');
        $listar->bindValue(":matricula",$matricula.'%');
        $listar->bindValue(":rg",$rg.'%');
        $listar->bindValue(":cpf",$cpf.'%');
        $listar->execute();

        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os NOVOS DADOS<br>";
    }

}

function listarDocumentosMetadados($id){
    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT distinct i.nome, a.nome, m.tipodocumento, a.datanascimento, a.matricula, a.cpf,
        a.rg, a.mae, a.pai, c.nome, c.nivel, c.anoinicio, c.anofim, c.situacao, m.tipodocumento, m.nome, m.anodocumento   
        from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Aluno a on m.aluno_id=a.id join 
        Curso c on m.curso_id=c.id join Instituicao i on m.instituicao_id = i.id where d.id = :id
        group by m.id,c.id,a.id,i.id");
        $listar->bindValue(":id",$id);
        $listar->execute();

        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os NOVOS DADOS<br>";
    }

}

function listarDados($nome="",$matricula="",$rg="",$cpf="",$mae="",$pai=""){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("Select distinct d.id,a.nome as aluno, u.nome as user, d.dataleitura, dp.titulo, count(i.docpes_id) as qtd from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Usuario u on u.id = d.user_id join Aluno a on a.id = m.aluno_id join DocumentoPessoal dp on dp.aluno_id = a.id join Imagem i on i.docpes_id = dp.id 
WHERE (upper(a.nome) LIKE upper(:nome)) and (upper(a.matricula) LIKE upper(:matricula)) and (upper(a.rg) LIKE upper(:rg)) and (upper(a.cpf) LIKE upper(:cpf)) and (upper(a.mae) like upper(:mae)) and (upper(a.pai) like upper(:pai))
GROUP BY d.id,a.nome, u.nome, d.dataleitura, dp.titulo, i.docpes_id
UNION
Select distinct d.id,a.nome as aluno, u.nome as user, d.dataleitura, dp.titulo, count(i.docaca_id) as qtd from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Usuario u on u.id = d.user_id join Aluno a on a.id = m.aluno_id join DocumentoAcademico dp on dp.aluno_id = a.id join Imagem i on i.docAca_id = dp.id
WHERE (upper(a.nome) LIKE upper(:nome)) and (upper(a.matricula) LIKE upper(:matricula)) and (upper(a.rg) LIKE upper(:rg)) and (upper(a.cpf) LIKE upper(:cpf)) and (upper(a.mae) like upper(:mae)) and (upper(a.pai) like upper(:pai))
GROUP BY d.id,a.nome, u.nome, d.dataleitura, dp.titulo, i.docaca_id;");
        
        $listar->bindValue(":nome","%".$nome."%");
        $listar->bindValue(":matricula","%".$matricula."%");
        $listar->bindValue(":rg","%".$rg."%");
        $listar->bindValue(":cpf","%".$cpf."%");
        $listar->bindValue(":mae","%".$mae."%");
        $listar->bindValue(":pai","%".$pai."%");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }

}


function documentoPorId($id){

    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT m.tipoDocumento, m.nome as metadadosNome, m.anoDocumento, i.nome as instituicaoNome,a.nome as alunoNome, a.dataNascimento, a.matricula, a.cpf, a.rg, a.mae, a.pai, c.nome as cursoNome, c.nivel, c.anoinicio, c.anofim, c.situacao from documentoDigital d join Metadados m on d.metadados_id = m.id join Instituicao i on m.instituicao_id = i.id join aluno a on m.aluno_id =a.id join curso c on a.id = c.aluno_id where d.id = :id");
        $listar->bindValue(':id', $id, PDO::PARAM_INT);
       $listar->execute();
         if($listar->rowCount()>0):
                
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
   
}



function imagens($titulo="",$aluno=""){
    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT i.imagem FROM Aluno a join DocumentoPessoal dp on a.id = dp.aluno_id join Imagem i on i.docpes_id = dp.id where upper(dp.titulo) like upper(:titulo) and upper(a.nome) like upper(:aluno) UNION Select  i.imagem from Aluno a join DocumentoAcademico dp on a.id = dp.aluno_id join Imagem i on i.docpes_id = dp.id where upper(dp.titulo) like upper(:tituloo) and upper(a.nome) like upper(:alunoo)");
   
        $listar->bindValue(":titulo",$titulo."%");
        $listar->bindValue(":aluno",$aluno."%");
        $listar->bindValue(":tituloo",$titulo."%");
        $listar->bindValue(":alunoo",$aluno."%");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}

function imagemPessoalAluno($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT i.imagem from Imagem i join documentodigital d on d.id = i.doc_id where d.id = :id");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
                return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
            
    } catch (PDOException $e) {
            echo "Erro ao listar as Imagens<br>";
    }
}

function teste(){
    $pdo=conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM  Imagem");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);
        
        //assthru(pg_fetch_array($lob));

       
    
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}


function getInstituicoes(){
    $pdo=conectarComPdo();
    try{
        $listar = $pdo->query("SELECT  nome FROM  Instituicao");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);
        
        //assthru(pg_fetch_array($lob));

       
    
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}

function getCursos(){
    $pdo=conectarComPdo();
    try{
        $listar = $pdo->query("SELECT nome FROM  Curso");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);
        
        //assthru(pg_fetch_array($lob));

       
    
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}

function getDossie($nome="",$instituicao="",$curso=""){

    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT i.nome as nomeI , a.nome , c.nome as nomeC , d.id from dossie d join instituicao i on i.id = d.instituicao_id join curso c on c.id = d.curso_id join aluno a on a.id = d.aluno_id WHERE upper(a.nome) like upper(:nome) and upper(i.nome) like upper(:instituicao) and upper(c.nome) like upper(:curso);");
   
        $listar->bindValue(":nome",$nome."%");
        $listar->bindValue(":instituicao",$instituicao."%");
        $listar->bindValue(":curso",$curso."%");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}

/*
function listarDocumentosDossie($id){
    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT i.nome, a.nome,c.nome,d.id from dossie d join instituicao i on i.id = d.instituicao_id join curso c on c.id = d.curso_id join aluno a on a.id = d.aluno_id WHERE upper(a.nome) like upper(:nome) and upper(i.nome) like upper(:intituicao) and upper(c.nome) like upper(:curso);");
   
        $listar->bindValue(":nome",$nome."%");
        $listar->bindValue(":intituicao",$intituicao."%");
        $listar->bindValue(":curso",$curso."%");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}*/

function listarDocumentosDossie($id){
    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT d.id, d.titulo, d.descricao, d.tipo, count(i.id) as qnt from documentodigital d 
join dossie dd on dd.id = d.dossie_id join Imagem i on i.doc_id = d.id where dd.id = :id group by d.id");  
        $listar->bindValue(":id",$id);
        
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}

function updateSenha($id,$senha){
	$pdo = conectarComPdo();
    $novaSenha = criptografar($senha);
    try{
        $listar = $pdo->prepare("UPDATE usuario SET  senha = :senha WHERE id = :id; ");
        $listar->bindValue(":id",$id);//."%"
         $listar->bindValue(":senha",$novaSenha);
        $listar->execute();
        
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}


function getAlunoDossieId($id){
    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT i.nome as nomeI, a.nome,c.nome as nomeC,d.id, d.classificacao from dossie d join instituicao i on i.id = d.instituicao_id join curso c on c.id = d.curso_id join aluno a on a.id = d.aluno_id WHERE  d.id = :id;");
   
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}
//$v = teste();
//header('Content-Type: image/png');
//foreach ($v as $key) {
    //print_r($key);
   // print_r($key['imagem']);
    //echo "<pre>"; var_dump($key['imagem']); echo "</pre>";
    //echo  stream_get_contents($key['imagem']);

//}

//fpassthru($v[0]->imagem);
//print_r($t);
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";


//$l=base64_encode((string)stream_get_contents($v[0]->imagem));
//print_r($l);

//echo "<img src='data:image/png;base64,".$l."' />";


//echo "oi<img src='C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg'>";
//$a = imagens("D","Gefe");
//foreach ($a as $key) {
    //echo "<img src=".".".$key->imagem.".png"."."."></img>";
//}
?>
